# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
NUS CVWO Assignment 1
Name: Huynh Thanh Duc Anh
Matric no: A0161312X 

### Set up database ###
- A mySQL dump file has been attached.
- Please edit database credentials in includes/config.php.

### Reader ###
- Browse and click on a specific post on homepage to view it.

### Writer (admin) ###
- Click on box "Write!" on homepage to login.
-- Demo account: username: demo, password: demo.
- Can add/edit/delete posts, add/edit users.
##### Notes for writers: #####
- User email can be 'null'.
- For each post:
-- Title, description, and content must not be 'null'.
-- The number of characters in Description must be less than 500.